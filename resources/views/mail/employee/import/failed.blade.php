<h3>Olá</h3>

<p>Houve algum problema com sua importação de colaboradores, por favor verifique os erros abaixo e faça o upload novamente.</p>


<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Document</th>
            <th>City</th>
            <th>State</th>
            <th>Start Date</th>
            <th>Errors</th>
        </tr>
    </thead>
    <tbody>
        @foreach($failures as $failure)
            <tr>
                <td>{{ $failure['row'] }}</td>
                <td>{{ $failure['values']['name'] }}</td>
                <td>{{ $failure['values']['email'] }}</td>
                <td>{{ $failure['values']['document'] }}</td>
                <td>{{ $failure['values']['city'] }}</td>
                <td>{{ $failure['values']['state'] }}</td>
                <td>{{ $failure['values']['start_date'] }}</td>
                <td>{{ implode(' | ', $failure['errors']) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
