<?php

namespace App\Import\Employee;

use App\Models\Employee;
use App\Notifications\Employee\ImportCompletedNotification;
use App\Notifications\Employee\ImportFailedNotification;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RemembersRowNumber;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\ImportFailed;

class EmployeesImport implements ToCollection, WithHeadingRow, WithValidation, WithChunkReading, WithEvents, ShouldQueue
{
    use Importable, RemembersRowNumber;

    public function __construct(private Authenticatable $authUser)
    {
    }

    public function collection(Collection $rows): void
    {
        foreach ($rows as $row) {
            $employee = Employee::firstOrNew([
                'user_id' => $this->authUser->id,
                'document' => $row['document']
            ]);

            $employee->name = $row['name'];
            $employee->email = $row['email'];
            $employee->city = $row['city'];
            $employee->state = $row['state'];
            $employee->start_date = $row['start_date'];

            $employee->save();
        }
    }

    public function rules(): array
    {
        return [
            'name' => 'required',
            'document' => 'required',
            'email' => 'required|email',
            'city' => 'required',
            'state' => 'required',
            'start_date' => 'required|date'
        ];
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function (ImportFailed $event) {
                $failures = collect($event->getException()->failures())->map(function ($failure) {
                    return [
                        'row' => $failure->row(),
                        'attribute' => $failure->attribute(),
                        'values' => $failure->values(),
                        'errors' => $failure->errors()
                    ];
                })
                ->toArray();

               $this->notifyUserImporFailed($failures);
            },
            AfterImport::class => function (AfterImport $event) {
               $this->notifyUserImportCompleted();
            }
        ];
    }

    public function notifyUserImportCompleted(): void
    {
        Notification::send($this->authUser, new ImportCompletedNotification());
    }

    public function notifyUserImporFailed(array $failures): void
    {
        Notification::send($this->authUser, new ImportFailedNotification($failures));
    }
}
