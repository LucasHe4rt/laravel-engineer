<?php

namespace App\Notifications\Employee;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ImportFailedNotification extends Notification
{
    use Queueable;

    public function __construct(private array $failures)
    {
        //
    }

    public function via(mixed $notifiable): array
    {
        return ['mail'];
    }

    public function toMail(mixed $notifiable): MailMessage
    {
        return (new MailMessage)
            ->from('no-reply@convenia.tech', 'Convenia')
            ->subject('Importação de colaboradores')
            ->view('mail.employee.import.failed', ['failures' => $this->failures]);
    }
}
