<?php

namespace App\Notifications\Employee;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ImportCompletedNotification extends Notification
{
    use Queueable;

    public function via(mixed $notifiable): array
    {
        return ['mail'];
    }

    public function toMail(mixed $notifiable): MailMessage
    {
        return (new MailMessage)
            ->from('no-reply@convenia.tech', 'Convenia')
            ->subject('Importação de colaboradores')
            ->greeting('Olá!')
            ->line('Sua solicitação de importação de colaboradores foi finalizada!')
            ->salutation('Atenciosamente, Convenia');
    }
}
