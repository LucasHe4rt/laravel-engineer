<?php

namespace Feature;

use App\Models\Auth\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('passport:install');
    }

    public function test_should_register_user()
    {
        // Prepare
        $user = User::factory()->make();
        $payload = [
            'name' => $user->name,
            'email' => $user->email,
            'password' => 'senha',
            'password_confirmation' => 'senha'
        ];

        // Act
        $response = $this->post(route('register'), $payload);

        // Assert
        $response->assertCreated();
        $this->assertDatabaseHas('users', [
            'name' => $user->name,
            'email' => $user->email
        ]);
    }

    public function test_should_login_user()
    {
        // Prepare
        $user = User::factory()->create();

        // Act
        $response = $this->post(route('login'), ['email' => $user->email, 'password' => 'password']);

        // Assert
        $response->assertOk();
        $response->assertJsonStructure(['user', 'access_token']);
        $this->assertAuthenticated();
    }

    public function test_should_not_login_user()
    {
        // Prepare
        $user = User::factory()->create();

        // Act
        $response = $this->post(route('login'), ['email' => $user->email, 'password' => '123123']);

        // Assert
        $response->assertUnauthorized();
        $response->assertJsonStructure(['message']);
        $this->assertGuest();
    }

    public function test_should_logout_user()
    {
        // Prepare
        $user = User::factory()->create();

        // Act
        $response = $this->post(route('login'), ['email' => $user->email, 'password' => 'password']);
        $responseLogout = $this->withHeaders(
            [
            'Authorization' => 'Bearer ' . $response->json('access_token')
            ]
        )
            ->post(route('logout'));

        // Assert
        $responseLogout->assertOk();
        $responseLogout->assertJsonStructure(['message']);
    }
}
